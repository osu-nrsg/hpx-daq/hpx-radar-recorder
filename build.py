"""build.py - Steps to download the WIMR format spec and CDL file needed by
hpx-radar-recorder"""

import json
import sys
import urllib.request as urllib2
from pathlib import Path

basedir = Path(__file__).resolve().parent
wimr_spec_dir = basedir / "hpx_radar_recorder" / "wimr-format-spec"
wsi_path = wimr_spec_dir / "wimr_spec_info.json"


class WimrSpecInstallError(Exception):
    pass


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def install_wimr_spec(spec_folder: Path = wimr_spec_dir):
    if not spec_folder.is_dir():
        try:
            spec_folder.mkdir(parents=True)
        except PermissionError:
            raise WimrSpecInstallError(
                f"You don't have permission to create dir {spec_folder}."
            ) from None
    with open(wsi_path, "r") as fobj:
        wimr_spec = json.load(fobj)
    try:
        files = wimr_spec["files"]
        tagname = wimr_spec["tagname"]
    except AttributeError:
        raise WimrSpecInstallError(
            "pyproject.toml file must contain a 'wimr-format-spec' section with 'files'"
            " and 'tag' values."
        ) from None

    raw_url = f"https://gitlab.com/osu-nrsg/wimr-format-spec/-/raw/{tagname}/"

    for filename in files:
        eprint(f"Downloading {filename} to {spec_folder}/ ...")
        file_url = raw_url + filename
        try:
            with urllib2.urlopen(
                urllib2.Request(file_url, headers={"User-Agent": "Mozilla"})
            ) as req:
                with open(Path(spec_folder, filename), "wb") as f:
                    f.write(req.read())
        except PermissionError:
            raise WimrSpecInstallError(
                f"You don't have permission to install the spec to {spec_folder}."
            ) from None
    eprint("Done installing the WIMR spec.")


install_wimr_spec()
