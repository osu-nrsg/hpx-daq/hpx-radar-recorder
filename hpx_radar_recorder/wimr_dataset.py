"""wimr_dataset.py - Contains the WIMRDataset class"""

import logging
import re
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path

import numpy as np
from hpx_client import ConfigMsg as ServerConfig
from hpx_client import FMConfig, Frame
from ncdataset import NCDataset
from nctemplate import NCTemplate

from hpx_radar_recorder._netcdf4 import (
    contains_vlen,
    get_createVar_kwargs,
)
from hpx_radar_recorder.logging import trace
from hpx_radar_recorder.parameters import (
    RadarParameters,
    RecordingParameters,
    SiteParameters,
)
from hpx_radar_recorder.version import __version__

logger = logging.getLogger(__name__)

ISO8601_SS_FMT = "%Y-%m-%dT%H:%M:%SZ"


def camel_case_split(identifier: str) -> list[str]:
    """Split camelCase strings into a list of words.
    From https://stackoverflow.com/a/37697078
    """
    return re.sub("([A-Z][a-z]+)", r" \1", re.sub("([A-Z]+)", r" \1", identifier)).split()


class WIMRDataset:
    def __init__(
        self,
        rec_params: RecordingParameters,
        site_params: SiteParameters,
        radar_params: RadarParameters,
        server_config: ServerConfig,
        fm_config: FMConfig,
        start_dt: datetime,
    ):
        self.nc_name = f"{site_params.station}_{start_dt:%Y%m%d_%H%M%S}.nc"
        rec_dir = rec_params.rec_dir
        if rec_params.date_dir:
            rec_dir = rec_dir / f"{start_dt:%Y-%m-%d}"
            rec_dir.mkdir(exist_ok=True)
        self.nc_path = rec_dir / self.nc_name
        self.ds = NCDataset(self.nc_path, keepopen=False, mode="w")
        self.start_dt = start_dt

        # Set up the WIMR file for recording
        # Copy the dimensions, variables, and attributes from the template to the dataset.
        self._duplicate_template(rec_params.template_cdl, self.ds, fm_config.nsamples)
        # Pre-set attributes and variables that can be set before the recording.
        self._preset_global_attrs(site_params, radar_params, server_config)
        self._preset_variable_attrs(site_params, server_config, fm_config)
        self._preset_variables(
            rec_params, site_params, radar_params, server_config, fm_config
        )

        # store some properties for later
        self.moving_platform = site_params.moving_platform
        self.heading = site_params.heading
        self.summing_factor = server_config.summing_factor

    @contextmanager
    def ds_context(self, **kwargs):
        """Context manager for the netCDF4 Dataset.

        Parameters
        ----------
        **kwargs
            Any keyword arguments for netCDF4.Dataset to use when re-opening the dataset,
            if it is not already open. Providing kwargs when the dataset is open will
            result in an error.
        """
        with self.ds(**kwargs) as ds:
            yield ds

    @staticmethod
    def _duplicate_template(template_cdl: Path, ds: NCDataset, nsamples: int):
        """Copy dimensions, variables, and attributes from the template, as verbatim as
        possible.

        Since range is the only changing fixed-size dimension in the dataset, `nsamples`
        is provided to set the size of that dimension, whereas the remainder of the
        dimension sizes are copied from the template.

        Some special assignments have to be done at this time (those affecting variable
        creation), but the rest of the updates for this recording can take place
        elsewhere.
        """

        with NCTemplate(template_cdl, mode="r") as template_ds, ds(mode="a"):
            # duplicate dimensions
            for dimname, dim in template_ds.dimensions.items():
                if dimname == "range":
                    size = nsamples
                elif dim.isunlimited():
                    size = None
                else:
                    size = dim.size
                ds.createDimension(dimname, size=size)

            # Duplicate variables and variable attributes
            for varname, var in template_ds.variables.items():
                chunksizes = [1, nsamples] if varname == "return_intensity" else None
                WIMRDataset._duplicate_var(varname, var, ds, chunksizes)

            # Duplicate global attributes
            for attrname in template_ds.ncattrs():
                ds.setncattr(attrname, template_ds.getncattr(attrname))

    @staticmethod
    def _duplicate_var(varname: str, template_var, ds, chunksizes):
        var_kwargs = get_createVar_kwargs(template_var)
        if not contains_vlen(template_var.datatype):
            # enable zlib compression and checksumming for all fixed-size variables.
            var_kwargs["zlib"] = True
            var_kwargs["contiguous"] = False  # required when using filters
            var_kwargs["complevel"] = 6
            var_kwargs["fletcher32"] = True
        # chunking should improve read/write efficiency if set correctly.
        if chunksizes is not None:
            var_kwargs["chunksizes"] = chunksizes
        # Create the var - note that it's OK to use var.dimensions as this is the
        # dimension _names_, not Dimension objects
        trace(
            logger,
            "Creating variable %s with datatype %s, dimensions %s and kwargs %s",
            varname,
            template_var.datatype,
            template_var.dimensions,
            var_kwargs,
        )
        ds_var = ds.createVariable(
            varname, template_var.datatype, template_var.dimensions, **var_kwargs
        )
        # Copy the variable attributes.
        for attrname in template_var.ncattrs():
            if attrname != "_FillValue":
                ds_var.setncattr(attrname, template_var.getncattr(attrname))

    def _preset_global_attrs(
        self,
        site_params: SiteParameters,
        radar_params: RadarParameters,
        server_config: ServerConfig,
    ):
        """Global attributes set before recording."""
        site_name = " ".join(camel_case_split(site_params.name))
        start_dtstr = self.start_dt.strftime(ISO8601_SS_FMT)
        moving = site_params.moving_platform
        attrs = {
            "title": (
                "Data recorded by"
                f" {site_params.institution} - {site_params.organization}"
                f" radar system on {start_dtstr} at the"
                f" {site_params.name} site."
            ),
            "history": f"{start_dtstr} - file created.",
            "instrument_name": radar_params.instrument_name,
            "site_name": site_name,
            "platform_is_mobile": "true" if moving else "false",
            "id": f"{site_params.name}_WIMR_{start_dtstr}",
            "acknowledgement": site_params.acknowledgement,
            "date_created": start_dtstr,
            "project": site_params.project,
            "platform": "ship" if moving else "station",
            "site_longname": site_params.long_name,
            "M_file": "",  # none for HPx recordings
            "computed_full_A_file": "true",
            "daq_hardware_type": f"Cambridge Pixel {server_config.card_name}",
            "site_version": site_params.version,
            "site_heading": 0 if moving else site_params.heading,
            "platform_heading_offset": site_params.platform_heading_offset,
            "site_donut": 1,
            "site_wave_dir_range": site_params.wave_dir_range,
            "processing_info": "",  # May be added externally.
            "software_versions": "\n".join(
                [
                    f"hpx-radar-server: {server_config.version}",
                    f"hpx-radar-recorder: {__version__}",
                ]
            ),
        }
        with self.ds(mode="a"):
            for attrname, attr in attrs.items():
                self.ds.setncattr(attrname, attr)

    def _preset_variable_attrs(
        self,
        site_params: SiteParameters,
        server_config: ServerConfig,
        fm_config: FMConfig,
    ):
        """Dynamically set some variable attributes before recording begins."""
        varattrs = {
            "azimuth": {
                "long_name": (
                    "azimuth_angle_from_platform_heading"
                    if site_params.moving_platform
                    else "azimuth_angle_from_true_north"
                ),
                "comment": (
                    "Azimuth of antenna relative to platform heading"
                    if site_params.moving_platform
                    else "Azimuth of antenna relative to true north"
                ),
            },
            "range": {
                "meters_to_center_of_first_gate": fm_config.rg[0],
                "meters_between_gates": fm_config.binsize,
            },
            "time": {"units": f"seconds_since {self.start_dt:%Y-%m-%dT%H:%M:%SZ}"},
            "return_intensity": {
                "valid_max": (2**server_config.bit_depth - 1)
                * server_config.summing_factor
            },
        }

        with self.ds(mode="a") as ds:
            for varname, attrs in varattrs.items():
                for attrname, attr in attrs.items():
                    ds[varname].setncattr(attrname, attr)

    def _preset_variables(
        self,
        rec_params: RecordingParameters,
        site_params: SiteParameters,
        radar_params: RadarParameters,
        server_config: ServerConfig,
        fm_config: FMConfig,
    ):
        """Variables written before recording."""
        moving = site_params.moving_platform
        start_dtstr = self.start_dt.strftime(ISO8601_SS_FMT)
        vars_ = {
            "latitude": -9999.0 if moving else site_params.latitude,
            "longitude": -9999.0 if moving else site_params.longitude,
            "altitude": site_params.altitude,
            # volume_number?
            "platform_type": "ship" if moving else "fixed",
            "instrument_type": "radar",
            "time_coverage_start": start_dtstr,
            "frequency": radar_params.frequency,
            "pulse_width": radar_params.pulse_width,
            "prt": 1 / radar_params.prf,
            "unambiguous_range": radar_params.unambiguous_range,
            "antenna_horizontal_beam_width": radar_params.horizontal_beam_width,
            "antenna_vertical_beam_width": radar_params.vertical_beam_width,
            "daq_sampling_frequency": server_config.adc_sampling_frequency,
            "daq_bit_depth": server_config.bit_depth,
            "daq_dc_offset": server_config.voltage_offset,
            "daq_attenuation": 0,
            "daq_gain": 10 * np.log10(np.abs(server_config.gain)),
            "daq_video_inverted": "inverted" if server_config.gain < 0 else "normal",
            "daq_trigger_delay": -9999,
            "daq_trigger_correction": server_config.range_correction_metres,
            "start_azi": fm_config.start_azi,
            "end_azi": fm_config.end_azi
            if rec_params.sweep_end_criterion == "end_azi"
            else -9999.0,
            "rays_per_sweep": (
                rec_params.summed_rays_per_sweep
                if rec_params.summed_rays_per_sweep > 0
                else -9999
            ),
            # Dimensioned
            "range": fm_config.rg,
        }
        with self.ds(mode="a"):
            for varname, var in vars_.items():
                self.ds[varname][...] = var

    def write_frame(self, frame: Frame, frame_index: int, start_ray_index: int):
        """Write a frame of data to the dataset

        Parameters
        ----------
        frame
            A frame (sweep) of radar data from hpx_client.FrameMaker
        frame_index
            0-based index of this frame
        """
        end_ray_index = start_ray_index + frame.nrays - 1
        ia = start_ray_index
        ib = end_ray_index
        azi_offset = 0.0 if self.moving_platform else self.heading
        self.ds["azimuth"][ia : ib + 1] = np.mod(frame.azi_smoothed + azi_offset, 360.0)
        elapsed_td = frame.time - np.datetime64(self.start_dt)
        self.ds["time"][ia : ib + 1] = (
            elapsed_td.astype("timedelta64[ns]").astype("int64") / 1.0e9
        )  # sec
        self.ds["return_intensity"][ia : ib + 1, :] = frame.data
        self.ds["sweep_start_ray_index"][frame_index] = start_ray_index
        self.ds["sweep_end_ray_index"][frame_index] = end_ray_index
        self.ds["sweep_number"][frame_index] = frame_index
        # some variables get constants
        self.ds["summing_factor"][frame_index] = self.summing_factor
        self.ds["sweep_mode"][frame_index] = "sector"
        self.ds["fixed_angle"][frame_index] = 0.0
        self.ds["elevation"][ia : ib + 1] = 0.0

    def finalize(self):
        """Write any data to the WIMR file that is not written until the recording is
        complete."""
        self._postset_vars()
        self._postset_global_attrs()

    def _postset_vars(self):
        """Write data of variable[s] after the recording is completed."""
        with self.ds(mode="a"):
            end_dt = self.start_dt + timedelta(seconds=self.ds["time"][:].data[-1])
            self.ds["time_coverage_end"][0] = end_dt.strftime(ISO8601_SS_FMT)

    def _postset_global_attrs(self):
        """Write some attributes after the recording is completed."""
        with self.ds(mode="a"):
            runtime_s = self.ds["time"][-1] - self.ds["time"][0]
            self.ds.setncattr("computed_run_time", runtime_s)
            self.ds.setncattr(
                "computed_TrueRPM", self.ds.dimensions["sweep"].size / (runtime_s / 60)
            )

    def delete(self):
        """Make sure the dataset is closed and then delete it."""
        if self.ds.isopen():
            self.ds.close()
        self.ds.nc_path.unlink()
