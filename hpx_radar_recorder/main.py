import logging
from collections.abc import Callable
from enum import IntEnum
from pathlib import Path
from typing import Optional, TypeAlias

import tomllib
import typer
from hpx_client import ServerStatusTimeout
from pydantic import ValidationError, validate_call
from typing_extensions import Annotated

from .logging import set_up_logging
from .parameters import RadarParameters, RecordingParameters, SiteParameters
from .recording import (
    Recording,
    RecordingConfigError,
    RecordingError,
)

logger = logging.getLogger(__name__)
PACKAGE_NAME = __name__.split(".")[0]

PathLike: TypeAlias = str | Path


_exit_code_docstrings = {
    0: "Recording finished successfully",
    1: "server stopped reporting status/config",
    2: "One of the parameters files is missing",
    4: "There is a validation error with the supplied parameters",
    5: "The supplied parameters cause an error configuring the recorder",
    6: (
        "The recording did not complete successfully (some sweeps may have been written"
        "to file)"
    ),
    255: "Other error",
}


class ExitCode(IntEnum):
    """Exit codes for cli"""

    OK = 0
    STATUS_TIMEOUT = 1
    MISSING_PARAMS = 2
    PARAMS_ERROR = 4
    CONFIG_ERROR = 5
    RECORDING_ERROR = 6
    UNKNOWN_ERROR = 255

    @property
    def _doc(self):
        return _exit_code_docstrings[self.value]

    @classmethod
    def doc(cls) -> str:
        return "ExitCode values:\n" + "\n".join(
            f"{v.value:3d} {v.name:15s} {v._doc}" for v in cls
        )


class Abort(typer.Exit):
    """Simple wrapper of typer. Abort exception with decoding of ExitCode"""

    def __init__(self, code: ExitCode):
        super().__init__(code.value)


@validate_call
def record(
    rec_params: RecordingParameters,
    site_params: SiteParameters,
    radar_params: RadarParameters,
    nsweep: int | None = None,
    frame_written_callback: Callable[[int], None] | None = None,
) -> Recording:
    """Record radar data from an HPx Radar Server, using the recording, site, and radar
    parameters in the specified input files.

    Parameters
    ----------
    rec_params
        RecordingParameters (see parameters.RecordingParameters)
    site_params
        SiteParameters (see parameters.SiteParameters)
    radar_params
        RadarParameters (see parameters.RadarParameters)
    nsweep
        Optional override of RecordingParameters.num_sweeps
    frame_written_callback
        Optional function that takes a single int argument (index of frame written) to run
        each time a frame is written

    Raises
    ------
    --any errors that may be raised by initializing or running Recording--

    Returns
    -------
    recording
        Completed Recording instance.
    """
    # Override nsweep if provided
    if nsweep:
        rec_params.num_sweeps = nsweep

    # Initialize the recording. It's possible that the server may not be running or there
    # is a logical problem in the configuration.
    logger.debug("Initializing recording.")
    recording = Recording(rec_params, radar_params, site_params, frame_written_callback)
    logger.debug("Recording intialized. Starting recording.")
    try:
        recording.run()
    except (Exception, KeyboardInterrupt) as ex:
        if recording.frames_written:
            logger.warning("Recording finished with errors -- data was written.")
        else:
            logger.warning("Recording was not started.")
        if isinstance(ex, KeyboardInterrupt):
            raise RecordingError(
                "The recording was canceled by a keyboard interrupt (Ctrl+C or SIGTERM)",
                recording=recording,
            ) from ex
        else:
            raise
    else:
        logger.info("Recording %s completed without errors.", recording.nc_name)

    return recording


def read_config(toml_path: PathLike):
    with open(toml_path, "rb") as fobj:
        return tomllib.load(fobj)


@validate_call
def record_cli(
    rec_params_file: PathLike,
    site_params_file: PathLike,
    radar_params_file: PathLike,
    nsweep: int | None = None,
) -> tuple[str, ExitCode, str]:
    """Record radar data from an HPx Radar Server, using the recording, site, and radar
    parameters in the specified input files.

    Parameters
    ----------
    rec_params_file
        Path to TOML file of RecordingParameters (see parameters.RecordingParameters)
    site_params_file
        Path to TOML file of SiteParameters (see parameters.SiteParameters)
    radar_params_file
        Path to TOML file of RadarParameters (see parameters.RadarParameters)
    nsweep
        Optional override of RecordingParameters.num_sweeps

    Returns
    -------
    nc_path
        Path to recording WIMR-format NetCDF dataset, presuming at least one sweep was
        written.
    exit_code
        Integer exit code. See ExitCode.doc()
    err_text
        Error text, if any.
    """
    nc_path = ""
    exit_code = ExitCode.OK
    err_text = ""
    # Load the parameters files. Any of them may result in a ParametersError
    try:
        logger.debug("Loading parameter files.")
        rec_params = RecordingParameters.model_validate(read_config(rec_params_file))
        site_params = SiteParameters.model_validate(read_config(site_params_file))
        radar_params = RadarParameters.model_validate(read_config(radar_params_file))
    except Exception as exc:
        exit_code, err_text = _parse_params_exc(exc)
        logger.exception(err_text)
        return "", exit_code, err_text
    logger.debug("Parameter files loaded.")

    nc_path = ""
    try:
        recording = record(
            rec_params,
            site_params,
            radar_params,
            nsweep,
            lambda i: print(f"{i + 1} frames written."),
        )
    except Exception as exc:
        exit_code, err_text = _parse_recording_exc(exc)
        logger.exception(err_text)
        if isinstance(exc, RecordingError) and exc.recording.frames_written:
            nc_path = str(exc.recording.nc_path)
    else:
        if recording.frames_written:
            nc_path = str(recording.nc_path)
    return nc_path, exit_code, err_text


def _parse_params_exc(exc: Exception) -> tuple[ExitCode, str]:
    if isinstance(exc, (tomllib.TOMLDecodeError, ValidationError)):
        exit_code = ExitCode.PARAMS_ERROR
        err_text = f"There is a problem in one of the parameters files:\n{exc}"
    elif isinstance(exc, FileNotFoundError):
        exit_code = ExitCode.MISSING_PARAMS
        err_text = f"A parameters file is missing: {exc.filename}"
    else:
        exit_code = ExitCode.UNKNOWN_ERROR
        err_text = str(exc)
    return exit_code, err_text


def _parse_recording_exc(exc: Exception) -> tuple[ExitCode, str]:
    if isinstance(exc, ServerStatusTimeout):
        exit_code = ExitCode.STATUS_TIMEOUT
        err_text = "No status messages received from the server. Is the server running?"
    elif isinstance(exc, RecordingConfigError):
        exit_code = ExitCode.CONFIG_ERROR
        err_text = f"There is a problem with the recording configuration.\n{exc}"
    elif isinstance(exc, RecordingError):
        exit_code = ExitCode.RECORDING_ERROR
        err_text = f"There was an error during the recording: {exc}"
    else:
        exit_code = ExitCode.UNKNOWN_ERROR
        err_text = str(exc)
    return exit_code, err_text


app = typer.Typer()


@app.command(
    help=(
        "Record radar data from an HPx Radar Server, using the recording, site, and radar"
        " parameters in the specified input files."
        f"\n\n\b\n{ExitCode.doc()}"  # print ExitCode doc without rewrap
    )
)
def cli(
    rec_params: Annotated[
        Path, typer.Argument(help="Path to the recording parameters file")
    ],
    site_params: Annotated[Path, typer.Argument(help="Path to the site parameters file")],
    radar_params: Annotated[
        Path, typer.Argument(help="Path to the radar parameters file")
    ],
    nsweep: Annotated[
        Optional[int],
        typer.Option(
            help=(
                "Number of sweeps to record, overriding the value in the"
                " RecordingParameters file"
            )
        ),
    ] = None,
    logfile: Annotated[
        Optional[Path],
        typer.Option(
            help="Path to write debug log, if specified. If not provided, logs to stdout"
        ),
    ] = None,
    loglevel: Annotated[
        str,
        typer.Option(
            help="Level to log in logfile [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
        ),
    ] = "DEBUG",
):
    """Record radar data from an HPx Radar Server, using the recording, site, and radar
    parameters in the specified input files.
    """
    set_up_logging(PACKAGE_NAME, logfile, loglevel)
    nc_path, exit_code, err_text = record_cli(
        rec_params, site_params, radar_params, nsweep
    )
    if nc_path:
        typer.echo(nc_path)
    if exit_code is not ExitCode.OK:
        typer.secho(err_text, fg=typer.colors.RED, err=True)
        raise Abort(code=exit_code)


if __name__ == "__main__":
    app()
