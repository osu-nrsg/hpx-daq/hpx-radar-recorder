"""_logging.py - Includes Logger class augmented with trace() and set_up_logging function
to set up logging for the whole package"""

import logging
from collections.abc import Mapping
from types import TracebackType
from typing import TYPE_CHECKING, Type

from typing_extensions import TypeAlias

# pulled from typeshed
_SysExcInfoType: TypeAlias = (
    tuple[Type[BaseException], BaseException, TracebackType] | tuple[None, None, None]
)
_ExcInfoType: TypeAlias = None | bool | _SysExcInfoType | BaseException

# monkey patch TRACE level into logging
TRACE = 5
logging._levelToName[TRACE] = "TRACE"
logging._nameToLevel["TRACE"] = TRACE

if TYPE_CHECKING:

    def trace(
        logger: logging.Logger,
        msg: object,
        *args: object,
        exc_info: _ExcInfoType = None,
        stack_info: bool = False,
        stacklevel: int = 1,
        extra: Mapping[str, object] | None = None,
    ) -> None:
        """
        Log 'msg % args' with severity 'TRACE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.info("Houston, we have a %s", "very specific problem", exc_info=1)
        """
else:

    def trace(logger, msg, *args, **kwargs):
        if logger.isEnabledFor(TRACE):
            logger._log(TRACE, msg, args, **kwargs)


def set_up_logging(package_name: str, filename=None, level: int | str = logging.DEBUG):
    package_logger = logging.getLogger(package_name)
    handler: logging.Handler
    if filename:
        handler = logging.FileHandler(filename)
        format_ = "{asctime}:{levelname}:{name} - {message}"
    else:
        handler = logging.StreamHandler()
        format_ = "{levelname}:{name} - {message}"
    formatter = logging.Formatter(format_, style="{")
    handler.setFormatter(formatter)
    handler.setLevel(level)
    package_logger.addHandler(handler)
    package_logger.setLevel(level)
