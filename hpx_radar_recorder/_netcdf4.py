from typing import TYPE_CHECKING, Any, Literal, TypedDict

import numpy as np

if TYPE_CHECKING:
    # These approximate types were determined by inspection of netCDF4-python v1.6.5 and
    # are just for use in this module
    class BloscInfo(TypedDict):
        compressor: str
        shuffle: int

    class SzipInfo(TypedDict):
        coding: str
        pixels_per_block: int

    class VarFilters(TypedDict):
        zlib: bool
        szip: Literal[False] | SzipInfo
        zstd: bool
        bzip2: bool
        blosc: Literal[False] | BloscInfo
        shuffle: bool
        complevel: int
        fletcher32: bool

    class Variable:
        _cmptype: Any | None  # netcdf4.CompoundType
        _enumtype: Any | None  # netCDF4.EnumType
        _grp: Any  # netCDF4.Dataset, netCDF4.Group
        _grpid: int
        _has_lsd: bool
        _iscompound: bool
        _isenum: bool
        _isprimitive: bool
        _isvlen: bool
        _name: str
        _ncstring_attrs__: bool
        _nunlimdim: int
        _use_get_vars: bool
        _varid: int
        _vltype: Any | None  # netcdf4.VLType

        always_mask: bool
        chartostring: bool
        datatype: Any  # dtype, CompoundType, EnumType, VLType
        dimensions: tuple[Any, ...]  # tuple of netcdf4.Dimension
        dtype: Any  # numpy.dtype
        name: str
        mask: bool
        ndim: int
        scale: bool
        shape: tuple
        size: int

        def assignValue(self, val): ...
        def chunking(self) -> Literal["contiguous"] | list[int]: ...
        def delncattr(self, name: str, value: Any): ...
        def endian(self) -> Literal["little", "big", "native"]: ...
        def filters(self) -> VarFilters: ...
        def getValue(self) -> Any: ...
        def get_dims(self) -> tuple: ...  # tuple of netcdf4.Dimension
        def get_var_chunk_cache(self) -> tuple[int, int, float]: ...
        def getncattr(self, name: str, encoding: str | None = "utf-8") -> Any: ...
        def group(self) -> Any: ...  # netcdf4.Group
        def ncattrs(self) -> list[str]: ...
        def quantization(self) -> tuple[int, str] | None: ...  # sig digits, quant mode
        def renameAttribute(self, oldname: str, newname: str): ...
        def set_auto_chartostring(self, chartostring: bool): ...
        def set_auto_mask(self, mask: bool): ...
        def set_auto_maskandscale(self, maskandscale: bool): ...
        def set_auto_scale(self, scale: bool): ...
        def set_collective(self, True_or_False: bool): ...
        def set_always_mask(self, ncstring_attrs: bool): ...
        def set_var_chunk_cache(
            self,
            size: int | None = None,
            nelems: int | None = None,
            preemption: float | None = None,
        ): ...
        def setncattr(self, name: str, value: Any): ...
        def setncattr_string(self, name: str, value: str): ...
        def setncatts(self, attdict: dict[str, Any]): ...
        def use_nc_get_vars(self, _use_get_vars: bool): ...
        def __getattr__(self, attrname: str) -> Any: ...

    class Dataset:
        def createVariable(
            self,
            varname,
            datatype,
            dimensions=(),
            compression=None,
            zlib=False,
            complevel=4,
            shuffle=True,
            szip_coding="nn",
            szip_pixels_per_block=8,
            blosc_shuffle=1,
            fletcher32=False,
            contiguous=False,
            chunksizes=None,
            endian="native",
            least_significant_digit=None,
            significant_digits=None,
            quantize_mode="BitGroom",
            fill_value=None,
            chunk_cache=None,
        ) -> Variable: ...

    class VLType:
        dtype: np.dtype
        name: str

    class CompoundType:
        dtype: np.dtype
        dtype_view: np.dtype
        name: str


else:
    from netCDF4 import CompoundType, Dataset, Variable, VLType  # noqa: F401


def get_createVar_kwargs(var: Variable) -> dict[str, Any]:
    """Use an existing `netCDF4.Variable` to get the keyword arguments for
    `netCDF4.Dataset.createVariable` to create an identical Variable

    Inputs
    ------
    var
        `netCDF4.Variable` that already exists in some (open) `netCDF4.Dataset`

    Returns
    -------
    kwargs
        Dict of keyword arguments to use with `netCDF4.Dataset.createVariable`
    """

    filters = var.filters()
    # copy these fields straight from `filters()`
    kwargs: dict[str, Any] = {
        filt_key: filters[filt_key] for filt_key in ["complevel", "shuffle", "fletcher32"]
    }
    # Any of these in filters() set the compression type
    for comptype in ["zlib", "zstd", "bzip2"]:
        if filters[comptype]:
            kwargs["compression"] = comptype
            break
    # blosc and szip details
    if filters["blosc"]:
        kwargs["compression"] = filters["blosc"]["compressor"]
        kwargs["blosc_shuffle"] = filters["blosc"]["shuffle"]
    if filters["szip"]:
        kwargs["compression"] = "szip"
        kwargs["szip_coding"] = filters["szip"]["coding"]
        kwargs["szip_pixels_per_block"] = filters["szip"]["pixels_per_block"]
    # chunking stuff
    if var.chunking() == "contiguous":
        kwargs["contiguous"] = True
    else:
        kwargs["chunksizes"] = var.chunking()
    # quantization stuff
    if hasattr(var, "least_significant_digit"):
        kwargs["least_significant_digit"] = var.least_significant_digit
    if quant := var.quantization():
        kwargs["significant_digits"] = quant[0]
        kwargs["quantize_mode"] = quant[1]
    # fill value
    if hasattr(var, "_FillValue"):
        kwargs["fill_value"] = var._FillValue
    return kwargs


def contains_vlen(datatype):
    """Determine if a `netCDF4.Variable` datatype is or contains (in a CompoundType) a
    variable-length datatype
    """
    # Workaround for https://github.com/Unidata/netcdf4-python/issues/1205
    if datatype == str:
        return True
    if isinstance(datatype, VLType):
        return True
    if isinstance(datatype, CompoundType):
        return _struct_dtype_has_vlen(datatype.dtype)


def _struct_dtype_has_vlen(dtype: np.dtype) -> bool:
    """Determine if a numpy structured datatype includes a flexible-length datatype in any
    of its fields"""
    assert dtype.names  # this function must always get structured datatypes!
    for fieldname in dtype.names:
        if dtype[fieldname].itemsize == 0:
            # flexible-length types (e.g. str) have an itemsize of 0
            return True
        elif dtype[fieldname].names:  # nested structured datatype
            return _struct_dtype_has_vlen(dtype[fieldname])
    return False
