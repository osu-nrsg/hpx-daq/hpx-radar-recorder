"""recording.py - This module contains the Recording class."""

import logging
from datetime import datetime, timedelta, timezone, tzinfo
from typing import Callable

import numpy as np
from hpx_client import FMConfig, FMConfigError, FrameMaker, ServerMonitor

from hpx_radar_recorder.wimr_dataset import WIMRDataset

from .parameters import RadarParameters, RecordingParameters, SiteParameters

logger = logging.getLogger(__name__)


class RecordingError(Exception):
    """General error type for hpx-radar-recorder Recordings"""

    def __init__(self, *args, recording: "Recording") -> None:
        super().__init__(*args)
        self.recording = recording


class RecordingConfigError(Exception):
    """Error type for hpx-radar-recorder Recording configuration"""

    pass


def dt64_to_dt(dt: np.datetime64, tz: tzinfo | None = None) -> datetime:
    """Convert numpy datetime64 to stdlib datetime.

    Note that stdlib datetime has us precision.

    Parameters
    ----------
    dt
        NumPy datetime64 scalar time (assumed to be naive)
    tz
        Optional timezone to set on the converted time (does not change time)

    Returns
    -------
    datetime
        Converted time
    """
    naive_dt = dt.astype("datetime64[us]").astype(datetime)
    return naive_dt if tz is None else naive_dt.replace(tzinfo=tz)


class Recording:
    """Main class for recording data from the HPx Radar Server

    Sets up the recording file, runs the recording, and saves the data to the recording
    file.

    Input parameters are documented under Recording.__init__().
    """

    def __init__(
        self,
        rec_params: RecordingParameters,
        radar_params: RadarParameters,
        site_params: SiteParameters,
        frame_written_callback: Callable[[int], None] | None = None,
        server_monitor: ServerMonitor | None = None,
    ):
        """Initialize the Recording.

        Parameters
        ----------
        rec_params
            Information about how this particular recording should be executed.
        radar_params
            Radar model metadata.
        site_params
            Radar site installation parameters.
        frame_written_callback
            Callable that accepts an integer argument called after a sweep is written to
            file. The sweep number is provided to the callable. Typical used for printing
            a message to stdout.
        server_monitor
            Instance of ServerMonitor (that may be already running) to improve startup
            time for repeated recordings.

        Raises
        ------
        RecordingConfigError
            There is a problem with the recording configuration.
        ServerStatusTimeout
            Status/config messages are not received while checking the server config. The
            server may not be running.
        """
        self.rec_params = rec_params
        self.radar_params = radar_params
        self.site_params = site_params
        self.frame_written_callback = frame_written_callback

        # Set up the ServerMonitor
        self.servmon = self._get_servmon(server_monitor)
        self._new_servmon = self.servmon is not server_monitor

        # Get an FMConfig and set up the FrameMaker
        try:
            self.fm_config: FMConfig = self._create_fmconfig(
                self.rec_params, self.servmon
            )
        except FMConfigError as fmc_err:
            raise RecordingConfigError(fmc_err.args[0]) from fmc_err
        self.frame_maker = FrameMaker(self.fm_config)  # not started yet.

        self._started = False  # To only allow one run per recording instance.
        self.frames_written = (
            False  # Set to true when at least one frame has been recorded
        )

    def _get_servmon(self, server_monitor: ServerMonitor | None) -> ServerMonitor:
        """Set up the server monitor, if necessary.

        Parameters
        ----------
        server_monitor
            An existing ServerMonitor instance to use. If its settings are different from
            those needed by the current recording it will be reset.

        Returns
        -------
        server_monitor
            A running ServerMonitor instance

        """
        if server_monitor:
            server_monitor.reset(
                self.rec_params.endpoint,
                self.rec_params.server_timeout_s,
                skip_if_unchanged=True,
            )
        return server_monitor or ServerMonitor(
            self.rec_params.endpoint, self.rec_params.server_timeout_s
        )

    @staticmethod
    def _create_fmconfig(
        rec_params: RecordingParameters, servmon: ServerMonitor
    ) -> FMConfig:
        """Create a FrameMaker config from RecordingParameters

        Parameters
        ----------
        rec_params
            RecordingParameters structure
        servmon
            ServerMonitor instance to use in FMConfig creation

        Returns
        -------
        FMConfig
            FrameMaker configuration

        Raises
        ------
        hpx_server_monitor.ServerStatusTimeout
            To check the range and azimuth bounds a config message is needed from the hpx
            server. If not received before the timeout, this exception is raised.
        hpx_framemaker.FMConfigError
            There is a problem with the configuration of the FrameMaker
        """
        return FMConfig(
            endpoint=rec_params.endpoint,
            start_azi=rec_params.start_azi,
            end_azi=(
                rec_params.end_azi
                if rec_params.end_azi not in [rec_params.start_azi, -9999.0]
                else None
            ),
            srays_per_frame=(
                rec_params.summed_rays_per_sweep
                if rec_params.summed_rays_per_sweep > 0
                else None
            ),
            min_range=(
                rec_params.start_range_m if rec_params.start_range_m >= 0 else None
            ),
            max_range=(rec_params.end_range_m if rec_params.end_range_m >= 0 else None),
            num_samples=(rec_params.num_samples if rec_params.num_samples >= 0 else None),
            servmon=servmon,
            # use defaults for these
            # frame_max_nrays_factor=2.0
            # framebuf_size=3
        )

    def run(self):
        """Run the recording.

        Many possible exceptions may be raised. A few are listed below:

        Raises
        ------
        RecordingError
            Some error occurred during the recording.
        HPxServerMonitorError
            There was some error regarding the server status or config.
        """
        if self._started:
            raise RecordingError(
                "Cannot run a Recording instance more than once.", recording=self
            )
        self._started = True
        logger.debug("Wait for status/config messages")
        with self.frame_maker:  # starts the frame maker
            try:
                self.servmon.wait_status_config()
                self.servmon.check_and_raise()
                self._init_recording()
                self._record()
            finally:
                self._postrecord()

    ###################
    # Private Methods #
    ###################
    def _init_recording(self):
        """Prepare for recording radar data."""
        logger.debug("Set up the recording file")
        self.start_dt = self._get_start_dt()
        self.wimr_ds = WIMRDataset(
            self.rec_params,
            self.site_params,
            self.radar_params,
            self.servmon.config,
            self.fm_config,
            self.start_dt,
        )

    @property
    def nc_name(self):
        try:
            return self.wimr_ds.nc_name
        except AttributeError:
            raise RecordingError(
                "The dataset name could not be retrieved because the dataset does not"
                " exist.",
                recording=self,
            ) from None

    @property
    def nc_path(self):
        try:
            return self.wimr_ds.nc_path
        except AttributeError:
            raise RecordingError(
                "The dataset path could not be retrieved because the dataset does not"
                " exist.",
                recording=self,
            ) from None

    def _get_start_dt(self) -> datetime:
        """Get a start time for the recording from the upcoming frame"""
        with self.frame_maker.next_frame(pop=False) as frame:
            return dt64_to_dt(frame.time[0], timezone.utc)

    def _record(self):
        """Get frames of data from the FrameMaker and write them to the WIMRDataset."""
        logger.debug("Start the recording")
        frame_index = -1
        t_elapsed = timedelta(0)
        start_ray_index = 0
        with self.wimr_ds.ds_context(mode="a"):
            while self._continue_recording(frame_index, t_elapsed):
                self.servmon.check_and_raise()  # make sure we can keep running
                if updated_vars := self._get_and_write_frame(
                    frame_index, start_ray_index
                ):
                    frame_index, start_ray_index, t_elapsed = updated_vars

    def _continue_recording(self, frame_index: int, t_elapsed: timedelta) -> bool:
        """Determine if recording should continue, based either on the number of sweeps
        requested in the recording or the requested duration of the recording.
        """
        if self.rec_params.num_sweeps > 0:  # nsweep-based recording length
            return (frame_index + 1) < self.rec_params.num_sweeps
        else:  # duration-based recording length
            return t_elapsed < self.rec_params.duration

    def _get_and_write_frame(
        self, frame_index: int, start_ray_index: int
    ) -> tuple[int, int, timedelta] | None:
        """Try to get a frame from the FrameMaker. On success, write it to the
        WIMRDataset, update the loop variables, and return them for the next go 'round.
        Otherwise return None.

        Parameters
        ----------
        frame_index
            Index in the sweep dimension to use for the next frame when writing it to the
            dataset.
        start_ray_index
            Index in the time dimension to use for first ray of the next frame when
            writing it to the dataset.

        Returns
        -------
        frame_index
            frame_index as above, updated for use with the next frame
        start_ray_index
            start_ray_index as above, updated for use with the next frame
        t_elapsed
            timedelta between the last ray of the written frame and the recording start
            time
        """
        with self.frame_maker.next_frame(timeout=0.1) as frame:
            if not frame:
                return None
            frame_index += 1
            self.wimr_ds.write_frame(frame, frame_index, start_ray_index)
            self.frames_written = True
            if self.frame_written_callback:
                self.frame_written_callback(frame_index)
            start_ray_index += frame.nrays
            t_elapsed = dt64_to_dt(frame.time[-1], timezone.utc) - self.start_dt
        return frame_index, start_ray_index, t_elapsed

    def _postrecord(self):
        """Execute post-recording tasks:

        - Delete the recording if no data was recorded, otherwise finalize the recording.
        - Shut down the server monitor, if a new one was created when initializing this
          Recording instance.
        """
        if self.frames_written:
            self.wimr_ds.finalize()
        else:
            logger.warning("No data recorded. Deleting recording file.")
            if getattr(self, "wimr_ds", None):
                # Delete the dataset, assuming it even got created in the first place
                self.wimr_ds.delete()
        if self._new_servmon:
            self.servmon.shutdown()
