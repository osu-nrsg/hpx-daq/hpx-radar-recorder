"""Contains the SiteParameters and RecordingConfig classes."""

from __future__ import annotations

import re
from datetime import timedelta
from pathlib import Path
from typing import Any, TypeAlias

from pydantic import (
    AfterValidator,
    BaseModel,
    DirectoryPath,
    Field,
    FilePath,
    model_validator,
)
from typing_extensions import Annotated
from validators.domain import domain
from validators.ip_address import ipv4, ipv6

NumericT: TypeAlias = int | float
numeric_t = (int, float)

#: Speed of light in a vacuum
c0 = 2.997e8

default_wimr_template_dir = Path(__file__).parent / "wimr-format-spec"
default_wimr_template_path = default_wimr_template_dir / "WIMRTemplate.cdl"


def xor(a: Any, b: Any) -> bool:
    return bool(a) ^ bool(b)


class RadarParameters(BaseModel):
    """Radar parameters (not part of DAQ configuration)

    Attributes
    ----------
    instrument_name : str
        Radar make and model
    prf : float
        Pulse Repetition Frequency, Hz.
    rpm_nominal : float
        Nominal rotation rate of the radar in rotations per minute.
    pulse_width : float
        Pulse width, seconds
    frequency : float
        Radar Carrier Frequency, Hz.
    horizontal_beam_width : float
        Antenna horizontal beam width (-3dB), degrees
    vertical_beam_width : float
        Antenna vertical beam width (-3dB), degrees
    """

    instrument_name: str
    prf: float
    rpm_nominal: float
    pulse_width: float
    frequency: float
    horizontal_beam_width: float
    vertical_beam_width: float

    @property
    def unambiguous_range(self):
        return c0 * (1 / self.prf - self.pulse_width) / 2


def valid_zeromq_address(v: str):
    """Validate ZeroMQ addresses. Only accept existing ipc sockets or tcp sockets with
    IPv4 host"""
    if not (match := re.match(r"(ipc|tcp)://(.+)", v)):
        raise ValueError(f"{v} is not a valid ZeroMQ endpoint for this application.")
    protocol, address = match.groups()
    if protocol == "ipc" and not Path(address).is_socket():
        raise ValueError(
            f"endpoint {v} does not point to an existing POSIX socket."
            " The radar server may not be running."
        )
    elif protocol == "tcp":  # tcp socket
        _validate_zmq_tcp_connect_address(address)
    return v


ZeroMQAddress = Annotated[str, AfterValidator(valid_zeromq_address)]


class RecordingParameters(BaseModel):
    """Container for the configuration of the radar recorder.

    Notes:
      * Either num_sweeps or duration must be defined.
      * Cannot define both end_azi and num_summed_rays

    Attributes
    ----------
    endpoint : str
        0MQ endpoint to connect to receive messages from the HPx Radar Server, e.g.
        ipc://<path_to_socket> or tcp://<ip>:<port>
    rec_dir : Path
        The recording file will go in this directory.
    date_dir : boolean, optional
        If True (default) the recording is saved in a folder like <rec_dir>/yyyy-mm-dd
    template_cdl : Path, optional
        Path to the NetCDF CDL file to use to generate the template NetCDF file for this
        recording. If not provided, the file is expected to be in the wimr-format-spec
        folder adjacent to where hpx-radar-recorder is installed.
    server_timeout_s : float, optional
        Time in seconds to allow without receiving a status message from the server.
        Default is 5 s.
    num_sweeps : int, optional
        Total number of rotations to record. -1 signifies to use duration instead
    duration : datetime.timedelta, optional
        Duration to record, rather than number of rotations. Seconds in config file.
    start_azi : float, optional
        Starting radar azimuth at which to record, in range [0, 360). Default is 0.
    end_azi : float, optional
        Exclusive upper boundary radar azimuth at which to record, in range [0, 360). If
        same as start_azi or if -9999.0, full 360° is recorded. Ignored if num_summed_rays
        is greater than 0.
    summed_rays_per_sweep : int, optional
        Number of summed rays to record in each sweep. If -1, end_azi is used instead.
    num_samples : int, optional
        Number of samples to record in each ray. Default is all samples from the server
        (cannot be more).
    start_range_m : float, optional
        Distance to nearest sample to keep. Defaults to server start range (cannot be
        nearer).
    end_range_m : float, optional
        Distance to furthest sample to keep. Defaults to full range from server. Cannot
        define both num_samples and end_range_m.
    """

    endpoint: ZeroMQAddress
    rec_dir: DirectoryPath
    date_dir: bool = True
    template_cdl: FilePath = default_wimr_template_path
    server_timeout_s: float = Field(default=2.0, gt=0)
    num_sweeps: int = -1
    duration: timedelta = timedelta(0)
    start_azi: float = 0.0
    end_azi: float = 0.0
    summed_rays_per_sweep: int = -1
    num_samples: int = -1
    start_range_m: float = -9999.0
    end_range_m: float = -9999.0

    @model_validator(mode="after")
    def check_one_of_num_sweeps_duration(self):
        if not xor(self.num_sweeps > 0, self.duration > timedelta(0)):
            raise ValueError(
                "Exactly one of num_sweeps or duration must be greater than 0."
            )
        return self

    @model_validator(mode="after")
    def check_one_of_num_samples_end_range_m(self):
        if self.num_samples > 0 and self.end_range_m > 0:
            raise ValueError("Can only define one of num_samples and end_range_m.")
        return self

    @model_validator(mode="after")
    def check_end_range_greater_than_start(self):
        if (
            self.start_range_m > 0
            and self.end_range_m > 0  # both are set
            and self.end_range_m < self.start_range_m
        ):
            raise ValueError("end_range_m must be greater than start_range_m")
        return self

    @property
    def sweep_end_criterion(self):
        return "end_azi" if self.summed_rays_per_sweep < 0 else "num_summed_rays"


class SiteParameters(BaseModel):
    """Class to represent the metadata parameters for a paritcular site.

    If moving_platform is True, all positional parameters are ignored as this information
    is expected to come from an outside source during processing.
    """

    name: str
    long_name: str
    station: str
    altitude: float
    heading: float
    latitude: float
    longitude: float
    moving_platform: bool = False
    platform_heading_offset: float = 0.0
    wave_dir_range: tuple[float, float] = (0.0, 360.0)
    version: int = 1
    comment: str = ""
    project: str = ""
    acknowledgement: str = ""
    institution: str = ""
    organization: str = ""


def _validate_zmq_tcp_connect_address(address: str):
    try:
        addr, separator, port = address.rpartition(":")
        if not separator:
            raise ValueError
        if (
            not (ipv4(addr) or ipv6(addr.strip("[]")) or domain(addr))
            or not 0 < int(port) <= 65535
        ):
            raise ValueError
    except (ValueError, TypeError):
        raise ValueError(f"{address} is not a valid tcp endpoint") from None
