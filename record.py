import logging

from nested_config import validate_config

import hpx_radar_recorder
from hpx_radar_recorder import RadarParameters, RecordingParameters, SiteParameters

handler = logging.StreamHandler()
logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
reclogger = logging.getLogger("hpx_radar_recorder")
reclogger.setLevel(logging.DEBUG)
reclogger.addHandler(handler)

REC_PARAMS = "params/example_recording_params.toml"
SITE_PARAMS = "params/TPG_site_params.toml"
RADAR_PARAMS = "params/TPG_radar_params.toml"


if __name__ == "__main__":
    hpx_radar_recorder.record(
        validate_config(REC_PARAMS, RecordingParameters),
        validate_config(SITE_PARAMS, SiteParameters),
        validate_config(RADAR_PARAMS, RadarParameters),
        nsweep=10,
        frame_written_callback=lambda i: print(f"Recorded frame {i}."),
    )
