# HPx Radar Recorder

App for making recordings of radar data from HPx Radar Server

## Installation

Install with `pip`, either from tar.gz release, GitLab URL, or the
[Haller PyPI](https://research.engr.oregonstate.edu/haller/pypi)):

```console
pip install --extra-index-url https://research.engr.oregonstate.edu/haller/pypi hpx-radar-recorder
```

## Usage

[HPx Radar Server](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server) must be installed and running.

By default, `hpx-radar-server` uses an IPC socket endpoint of `ipc:///run/hpx-radar-server/hpx-pub.sock`. A
TCP endpoint may also be used (see `hpx-radar-server -h`).

Once installed, `hpx-radar-recorder` may be run from the command line using the form listed in
`hpx-radar-recorder --help`:

```text
Usage: hpx-radar-recorder [OPTIONS] REC_PARAMS SITE_PARAMS RADAR_PARAMS

  Record radar data from an HPx Radar Server, using the recording, site, and
  radar parameters in the specified input files.

Arguments:
  REC_PARAMS    Path to the recording parameters file  [required]
  SITE_PARAMS   Path to the site parameters file  [required]
  RADAR_PARAMS  Path to the radar parameters file  [required]

Options:
  --nsweep INTEGER                Number of sweeps to record, overriding the
                                  value in the RecordingParameters file

  --logfile PATH                  Path to write debug log, if specified.
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.

  --help                          Show this message and exit.
```

## Parameters

See the [the params folder](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/tree/master/params) in
the git repository for example parameters files.

In the recording parameters file, note:

- One and only one of `num_sweeps` and `duration` may be specified
- At most one of `end_azi` and `summed_rays_per_sweep` may be specified
- `start_range_m`, `end_range_m`, and `num_samples` must be compatible with one another.
