# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased-dev]

## [Unreleased]

## [1.5.0] - 2024-05-31

### Changes

- Updated dependency minimum versions:
  - Python 3.12
  - nctemplate 1.1.1
  - hpx-client 2.3.2
  - numpy 1.26.4
- Update `pyproject.toml` and build.py for newer Poetry compatibility.
- wimr spec
  - Updated to v1.7.1
  - `wimr-format-spec` is now saved in the package dir, not outside of it
- Refactor the `Recording` class.
  - Break out new WIMRDataset class to just manage writing the WIMR dataset.
  - Update attribute and method names to make a little more sense.
  - Move functions out of `util.py` and into the modules where they're used.
  - Improve docstrings
- Various other docstring improvements and minor refactors

## [1.4.3] - 2024-04-26

### Changed

- Upgraded to **typer** v0.12.3, now using `typing.Annotated` for help
- Max line length is now 90

### Fixed

- No longer using deprecated `datetime.utcfromtimestamp`

## [1.4.2] - 2024-04-24

### Added

- Logging includes `trace()` level
- new `set_up_logging` fn
- Log to stdout if no logfile specified

### Changed

- Switch to **ruff** for formatting

### Fixed

- Update to work with newer netCDF4 api. Minimum netCDF4 version is now 1.6

## [1.4.1] - 2024-04-22

- Use latest Pydantic
- Use Pydantic 2 style validators
- Remove unneeded nested-confg

## [1.4.0] - 2024-04-10

- Update to pydantic 2
- Update nested-config to 2.0.1
- Update hpx-client to 2.2.2
- Fix validators to work with pydantic 2

## [1.3.1] - 2024-04-09

- Change to nested-config from pydantic-plus. Use pydantic BaseModel
- Update build script to not need toml
- Fix ExitCode enum to type properly

## [1.3.0] - 2022-09-29

- Refactor: Move `__version__` to `version.py`
- Refactor: Move main `logger` to `_logging.py`
- Refactor: Remove unused attributes
- Refactor: ServerMonitor starts on creation. If a new one is created, shut it down after recording.
- Documentation: Add/modify comments and docstrings
- Refactor: Update to hpx_client 2.2.0
- Refactor: Python 3.9 - use native types (list, dict, etc.)
- Refactor: Pin netCDF4-python to 1.5.x until variable creation issue can be addressed (See issue #11.)
- Add mypy
- Refactor: Improve exception handling and cli
- Use non-pop option on FrameMaker to use the current frame for the start datetime -- save startup time

## [1.2.6] - 2022-02-24

- BUGFIX: Make sure `self.ds` exists in `_postrecord`

## [1.2.5] - 2022-02-09

- Fix missing ServerStatusTimeout import

## [1.2.4] - 2022-02-09

- Minor typing, simplification, and logical refactoring/fixes
- Simplify zeromq socket address validator

## [1.2.3] - 2021-08-05

- Revert to pydantic-plus

## [1.2.2] - 2021-08-04

- Fix order of classmethod and validate_arguments decorators in BaseModel

## [1.2.1] - 2021-08-03

- Replace pydantic-plus with local module.

## [1.2.0]

- Replace hpx_server_monitor, hpx_messages, and hpx_framemaker with hpx_client.
- Update pydantic-plus now using rtoml.
- Remove zmq_quickline as pyzmq now has context manager for connect and bind

## [1.1.1]

- Site altitude saved even for moving platform

## [1.1.0]

- If endpoint socket file is missing warn that server might not be running.
- Add date_dir option to RecordingParams
- Separate CLI from main:record()
- Rework logging a bit to mostly just catch errors and log when using the CLI.

## [1.0.3]

- Use pydantic-plus

## [1.0.2]

- Add ability to specify frame_written_callback in `record()`, rather than automatically print frame index
- Runtime arg validation of `record()` with pydantic

## [1.0.1]

- Updated pyproject.toml to ensure toml and setuptools are available for install.
- Updated main to display exit codes with --help
- Removed build_and_publish, added simple build script.

## [1.0.0] - 2021-03-23

### Modified

- Fix failure trying to close/delete uncreated dataset
- Added `hpx-radar-recorder` cli app
- Added `frame_written_callback` argument to `Recording.__init__` to provide custom logging function.
- Improved error handling for parent app handling
- More docstrings added to fns
- Use `pydantic` for standardized conversion from toml file to dataclass.
- Add better logging
- #4 Auto-download WIMR template on install
- #5 Handle negative HPx gain
- HPx Framemaker 0.4.0 - range and azimuth setting validation (vs. server settings) included therein

### Added

- Furuno radar params
- #6 Save `software_versions` attr in NetCDF file
- #3 CHANGELOG and `single-version` usage
- #7 Add README

## [0.1.0] - 2020-11-02

- Initial release

[Unreleased-dev]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.5.0...dev
[Unreleased]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.5.0...master
[1.5.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.4.3...v1.5.0
[1.4.3]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.4.2...v1.4.3
[1.4.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.4.1...v1.4.2
[1.4.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.4.0...v1.4.1
[1.4.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.3.1...v1.4.0
[1.3.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.3.0...v1.3.1
[1.3.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.6...v1.3.0
[1.2.6]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.5...v1.2.6
[1.2.5]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.4...v1.2.5
[1.2.4]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.3...v1.2.4
[1.2.3]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.2...v1.2.3
[1.2.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.1...v1.2.2
[1.2.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.0.3...v1.1.0
[1.0.3]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/compare/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-recorder/-/tags/v0.1.0
